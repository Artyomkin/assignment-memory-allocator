#include <stdio.h>

#define HEAP_SIZE 5000

#define MALLOC_SIZE 1000

#define FIRST_BLOCK 1000
#define SECOND_BLOCK 2000
#define THIRD_BLOCK 3000
#define FOURTH_BLOCK 7000
#define BIG_BLOCK 50000

#include <unistd.h>
#define MAP_ANONYMOUS 0x20

#include "mem.h"
#include "mem_internals.h"

void *heap;
struct block_header *head;

inline static int get_freed_blocks() {
    int freed = 0;
    struct block_header *next = head;
    while (next) {
        if (next->is_free){
          freed++;  
        } 
        next = next->next;
    }
    return freed;
}

bool test1() {
    debug_heap(stdout, head);
    void *mem = _malloc(MALLOC_SIZE);
    debug_heap(stdout, head);
    if (mem == NULL || head->capacity.bytes != MALLOC_SIZE) {
        printf("test 1 failed");
        return false;
    }
    _free(mem);
    return true;
}

bool test2() {
    debug_heap(stdout, head);
    void *block1 = _malloc(FIRST_BLOCK);
    void *block2 = _malloc(SECOND_BLOCK);
    debug_heap(stdout, head);
    _free(block1);
    debug_heap(stdout, head);
    if (get_freed_blocks() != 2){
        printf("test 2 failed");
        return false;
    }
    _free(block2);
    _free(block1);

    return true;
}


bool test3() {
    debug_heap(stdout, head);
    void *block1 = _malloc(FIRST_BLOCK);
    void *block2 = _malloc(SECOND_BLOCK);
    void *block3 = _malloc(THIRD_BLOCK);
    debug_heap(stdout, head);
    _free(block1);
    _free(block2);
    debug_heap(stdout, head);
    if (get_freed_blocks() != 3){
        printf("test 3 failed");
        return false;
    } 
    _free(block3);
    _free(block2);
    _free(block1);
    return true;
}


bool test4() {
    debug_heap(stdout, head);
    void *block1 = _malloc(FOURTH_BLOCK);
    void *block2 = _malloc(SECOND_BLOCK);
    debug_heap(stdout, head);
    int allocated = 0;
    struct block_header *next = head;
    while (next->next) {
        next = next->next;
        allocated++;
    }
    if (allocated != 2) {
        printf("failed 4 test");
        return false;
    }
    _free(block2);
    _free(block1);
    return true;
}


bool test5() {
    head->capacity.bytes = HEAP_SIZE;
    debug_heap(stdout, head);

    struct block_header *next_block = head;
    while (next_block){
        next_block = next_block->next;
    } 

    size_t count = (size_t) next_block / sysconf(_SC_PAGESIZE);
    size_t remains = (size_t) next_block % sysconf(_SC_PAGESIZE);
    uint8_t *mem = (uint8_t * )(sysconf(_SC_PAGESIZE) * (count + (remains > 1)));
    (void) mmap(mem, 1000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);

    void *allocated = _malloc(BIG_BLOCK);

    if (mem == (uint8_t * )((uint8_t *) allocated - offsetof(struct block_header, contents))){
        printf("test 5 failed");
        return false;
    } 

    debug_heap(stdout, head);
    _free(allocated);

    return true;
}

typedef bool (*test)(void);

test tests[5] = {&test1, &test2, &test3, &test4, &test5};

inline static void print_divider() {
    for (int i = 0; i < 40; i++) printf("=");
    printf("\n");
}

bool test_all() {
    heap = heap_init(HEAP_SIZE);
    head = (struct block_header *) heap;

    bool res = true;

    for (size_t i = 0; i < 5; i++) {
        print_divider();
        printf("TEST %zu\n", i + 1);
        bool res_i = tests[i]();
        if (res_i) {
            printf("\nTEST %zu PASSED\n", i + 1);
        } else {
            printf("\nTEST %zu FAILED\n", i + 1);
        }
        res &= res_i;
        print_divider();
    }

    return res;
}


int main() {
    bool res = test_all();
    if (!res) {
        printf("TESTS FAILED\n");
        return 1;
    }

    printf("TESTS PASSED\n");
    return 0;
}
